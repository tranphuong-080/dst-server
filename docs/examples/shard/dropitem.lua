Title("friendly中立生物"),
	{
	  name = "niumao",
	  label = "beefalo牛掉落牛毛",
	  hover = "beefalo → beefalowool",
	   options =
        {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "jidan",
	  label = "gobbler火鸡掉落蛋",
	  hover = "gobbler → egg",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "huzi",
	  label = "rabbit兔子掉落胡须",
	  hover = "rabbit → beardhair",
	   options =
        {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "maowei",
	  label = "catcoon浣猫掉落尾巴",
	  hover = "catcoon → coontail",
	   options =
        {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "niaodan",
	  label = "birds鸟掉羽毛",
	  hover = "birds → feather",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 40},
			{description = "slow 慢速", data = 20},
			{description = "normal 正常", data = 3},
			{description = "fast 快速", data = 2},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 3,

	},

	{
	  name = "huolong",
	  label = "saladmander沙拉曼蛇掉火龙果",
	  hover = "saladmander → dragonfruit",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "zhupi",
	  label = "pigman猪人掉落猪皮",
	  hover = "pigman → pigskin",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "futeyang",
	  label = "voltgoat伏特羊掉落羊角",
	  hover = "voltgoat → goathorn",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "wuyanlu",
	  label = "deer无眼鹿掉落角(1%概率克劳斯钥匙)",
	  hover = "deer → deerantler(1%klaussackkey)",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},




	Title("hostile敌对生物"),
	{
	  name = "zhuwang",
	  label = "spider蜘蛛掉落蜘蛛网",
	  hover = "spider → silk",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 170},
			{description = "slow 慢速", data = 90},
			{description = "normal 正常", data = 60},
			{description = "fast 快速", data = 40},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 60,

	},

	{
	  name = "gaojiaoniaodan",
	  label = "tallbird高脚鸟掉落蛋",
	  hover = "tallbird → tallbird egg",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "chilun",
	  label = "knight发条骑士掉落齿轮",
	  hover = "knight → gears",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "gouya",
	  label = "hound猎犬掉落犬牙",
	  hover = "hound → hounds tooth",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "yingguang",
	  label = "skittersquid乌贼掉落荧光果",
	  hover = "skittersquid → lightbulb",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "gangyang",
	  label = "ewecus钢羊掉落钢丝绒",
	  hover = "ewecus → steelwool",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "emao",
	  label = "mossling小麋鹿鹅掉落毛",
	  hover = "mossling → goose feather",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "huomu",
	  label = "treeguard树人掉落活木",
	  hover = "treeguard → livinglog",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},


	Title("caves洞穴生物"),
	{
	  name = "heipi",
	  label = "slurper啜食者掉落皮",
	  hover = "slurper → slurper_pelt",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

	{
	  name = "tumao",
	  label = "bunnyman兔人掉落兔毛",
	  hover = "bunnyman → manrabbit_tail",
	  options =
       {
			{description = "close关闭", data = 0},
			{description = "very slow 超慢", data = 150},
			{description = "slow 慢速", data = 70},
			{description = "normal 正常", data = 40},
			{description = "fast 快速", data = 20},
			{description = "very fast 超快", data = 1},
     	 },
	    default = 40,

	},

}
