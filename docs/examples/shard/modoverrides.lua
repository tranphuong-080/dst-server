return {

	['workshop-1418878027'] = { enabled = false }, -- green
	['workshop-1484294726'] = { enabled = false }, -- Tropical Experience Complement
	['workshop-1505270912'] = { enabled = false, -- Tropical Experience
		configuration_options={
			['World Generation']=10
		}
	},
	['workshop-887967658'] = { enabled = false }, -- Magic store
	['workshop-726432903'] = { enabled = false }, -- multiworld
	['workshop-656256171'] = { enabled = false }, -- slot machine
	['workshop-1290063173'] = { enabled = false }, -- green world



	['workshop-551324730'] = { enabled = false },	-- near placement

	['workshop-1535331014'] = { enabled = true },	-- Place Anything closer
	['workshop-345692228'] = { enabled = false },	-- minimap
	['workshop-376333686'] = { enabled = false },	-- status
	['workshop-375859599'] = { enabled = false },	-- heath info
	['workshop-347079953'] = { enabled = false },	-- food value
	['workshop-666155465'] = { enabled = false },	-- show me
	['workshop-488009136'] = { enabled = true },	-- bow
	['workshop-356930882'] = { enabled = true },	-- infinit tent
	['workshop-396026892'] = { enabled = false },	-- lager chest
	['workshop-356420397'] = { enabled = false },	-- no respawn pennaty
	['workshop-359479220'] = { enabled = true },	-- Sollyz Character
	['workshop-379628839'] = { enabled = true },	-- medic Character
	['workshop-791838548'] = { enabled = true },	-- engine Character
	['workshop-380122571'] = { enabled = true },	-- dead
	['workshop-361994110'] = { enabled = true },	-- bee nice
	['workshop-387716835'] = { enabled = true },	-- aqua
	['workshop-385006082'] = { enabled = true },	-- light path
	['workshop-354533909'] = { enabled = true },	-- elephan
	['workshop-392369197'] = { enabled = true },	-- artis Character
	['workshop-522117250'] = { enabled = true },	-- tree
	['workshop-422744946'] = { enabled = true },	-- walter
	['workshop-1049176456'] = { enabled = true },	-- The Scout
	['workshop-589167205'] = { enabled = true },	-- The spide
	['workshop-371920345'] = { enabled = true },	-- auto catch bumerang
	['workshop-665141515'] = { enabled = true },	-- solider char
	['workshop-516523980'] = { enabled = true },	-- mine machine
	['workshop-762265715'] = { enabled = true },	-- wande char
	-- ['workshop-402531514'] = { enabled = true },	-- heavy char
	['workshop-363427869'] = { enabled = true },	-- Clementine
	['workshop-858801295'] = { enabled = true },	-- ice maker


	['workshop-922966487'] = { enabled = true },	-- start base
	['workshop-569043634'] = { enabled = false },	-- camfire respawn
	['workshop-1349799880'] = { enabled = false },	-- basement
	['workshop-544126369'] = { enabled = true },	-- random wave

	['workshop-1283844026'] = { enabled = false },	-- regrow

	['workshop-462434129'] = { enabled = false },	-- restart
	['workshop-661253977'] = { enabled = false },	-- dont drop everything

	['workshop-796742922'] = { enabled = false },	-- my home
	['workshop-514078314'] = { enabled = true },	-- Turfed
	['workshop-436654027'] = { enabled = true },	-- buffallo milk

	["workshop-706633455"]={ configuration_options={ speedmultiplier=2 }, enabled=true },	-- fast ghost

	['workshop-1153998909'] = { enabled = true },	-- armor
	['workshop-519266302'] = { enabled = true },	-- season start
	['workshop-768885735'] = { enabled = false },	-- Wooden Hut  not work for rain?
	['workshop-398858801'] = { enabled = false },	-- afk detect
	['workshop-416991215'] = { enabled = false },	-- music
	['workshop-1269877929'] = { enabled = false },	-- wall gate
	['workshop-929427619'] = { enabled = true },	-- weapon

	['workshop-375850593'] = { enabled = true },	-- extra equip
	['workshop-697356989'] = { enabled = true },	-- Shipwrecked Characters
	['workshop-370373189'] = { enabled = true },	-- advance farm

	['workshop-1248028833'] = { enabled = true },	--shield
	['workshop-346962876'] = { enabled = false },	-- Freezer
	['workshop-353697884'] = { enabled = false }, -- Craftable Gears
	['workshop-488018406'] = { enabled = true },	-- break ice
	['workshop-599498678'] = { enabled = true },	-- tool are fuel
	['workshop-787954095'] = { enabled = true },	-- Magic Bottle Lanterns
	['workshop-384396097'] = { enabled = true },	-- Spider Cage
	['workshop-404983266'] = { enabled = false }, -- Pickle It
	['workshop-354415247'] = { enabled = true },	-- throw spea
	['workshop-1079538195'] = { enabled = true },	-- Moving Box
	['workshop-346968521'] = { enabled = false },	-- house
	['workshop-447092740'] = { enabled = true },	-- more action
	['workshop-382177939'] = { enabled = false },	-- Storm Cellar
	['workshop-458587300'] = { enabled = false },	-- fast travel
	['workshop-378160973'] = { enabled = true,	-- globalposition
		configuration_options={
			SHOWPLAYERSOPTIONS=1,
			SHAREMINIMAPPROGRESS=true,
			SHOWPLAYERICONS=true ,
			ENABLEPINGS=true
		}
 	}
}
